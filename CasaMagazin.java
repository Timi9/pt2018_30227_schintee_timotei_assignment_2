package casa_magazin;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import client.Client;


public class CasaMagazin extends Thread {  //implement Runnable
	private int total_clienti_veniti_la_casa;
	
	private int idCasa;
	private ReentrantLock lacat;  // pentru a bloca un thread
	private BlockingQueue<Client> clienti_magazin;
	private final float asteptare_thread=600;   
	public String mesaj_afisare_consola=new String();

	public CasaMagazin(int id,ReentrantLock lacat,BlockingQueue<Client> clienti_magazin) {
		this.idCasa = id;
		this.lacat=lacat;
		this.clienti_magazin=clienti_magazin;
	}
	
public void run() {
		
		while(clienti_magazin.isEmpty()==false){
		Client c=clienti_magazin.poll();  // stie sa ia un om din blocking queue
		System.out.println("A intrat un client in magazin (Clientul nr: "+c.getId()+")");
		this.mesaj_afisare_consola=this.mesaj_afisare_consola+"A intrat un client in magazin (Clientul nr: "+c.getId()+")"+"\n";
		this.lacat.lock();
		timpPetrecutInMagazin(c);
		System.out.println("Casa "+idCasa+": A intrat un client la casa (Clientul nr: "+c.getId()+")");
		this.total_clienti_veniti_la_casa++;
		this.mesaj_afisare_consola=this.mesaj_afisare_consola+"Casa "+idCasa+": A intrat un client la casa (Clientul nr: "+c.getId()+")"+"\n";
		try {
			timpPetrecutLaCoada(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.lacat.unlock();
		System.out.println("Casa "+idCasa+": A iesit Clientul nr: "+c.getId()+" din magazin");
		this.mesaj_afisare_consola=this.mesaj_afisare_consola+"Casa "+idCasa+": A iesit Clientul nr: "+c.getId()+" din magazin"+"\n";
		System.out.println("Va multumim ca ati venit la noi in magazin");
		this.mesaj_afisare_consola=this.mesaj_afisare_consola+"Va multumim ca ati venit la noi in magazin"+"\n";
		}
		System.err.println("La Casa"+this.idCasa+" au fost in total:"+this.total_clienti_veniti_la_casa+" clienti"+"\n");
	}
	
	private void timpPetrecutInMagazin(Client client) {
		int timp_in_magazin;
		
		timp_in_magazin = (int) Math.round(Math.random() * client.getMaxTimpInMagazin());
		for (int i = 0; i < timp_in_magazin * 100000; i++) {
			i=i+5;
			i=i-3;		
			}
	}

	private void timpPetrecutLaCoada(Client client) throws Exception { 
		try {
			Thread.sleep(Math.round(Math.random() * (client.getMaxTimpLaCoada() * asteptare_thread+100)));
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		finally {
			
		}
	}
	
	public int afisare_clienti_veniti_la_casa() {
		return this.total_clienti_veniti_la_casa;
	}

	public int getTotal_clienti_veniti_la_casa() {
		return total_clienti_veniti_la_casa;
	}

	public void setTotal_clienti_veniti_la_casa(int total_clienti_veniti_la_casa) {
		this.total_clienti_veniti_la_casa = total_clienti_veniti_la_casa;
	}

	public int getIdCasa() {
		return idCasa;
	}

	public void setIdCasa(int idCasa) {
		this.idCasa = idCasa;
	}

	public ReentrantLock getLacat() {
		return lacat;
	}

	public void setLacat(ReentrantLock lacat) {
		this.lacat = lacat;
	}

	public BlockingQueue<Client> getClienti_magazin() {
		return clienti_magazin;
	}

	public void setClienti_magazin(BlockingQueue<Client> clienti_magazin) {
		this.clienti_magazin = clienti_magazin;
	}

	public String getMesaj_afisare_consola() {
		return mesaj_afisare_consola;
	}

	public void setMesaj_afisare_consola(String mesaj_afisare_consola) {
		this.mesaj_afisare_consola = mesaj_afisare_consola;
	}

	public float getAsteptare_thread() {
		return asteptare_thread;
	}
	
	

}
