package client;


import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class PopulareDeClienti {

	private final int random_aux=1;
	private int timp_magazin;
	private int timp_coada;

	private int numarClienti;
	private BlockingQueue<Client> lista_clienti;
	
	public PopulareDeClienti() {
		
	}
	
	public PopulareDeClienti(BlockingQueue<Client> lista_clienti) {
		this.lista_clienti=lista_clienti;
	}

	public PopulareDeClienti(int numarClienti,int timp_magazin,int timp_coada) {
		this.numarClienti = numarClienti;
		this.timp_coada=timp_coada;
		this.timp_magazin=timp_magazin;
		lista_clienti = new LinkedBlockingQueue<Client>();
		
		for (int i = 0; i < numarClienti; i++) {
			int id=0;
			Client aux_client=new Client();
			aux_client.setId(i);
			aux_client.setMaxTimpInMagazin(genereazaUnNumarRandom(timp_magazin)+1);
			aux_client.setMaxTimpLaCoada(genereazaUnNumarRandom(timp_coada)+1);
			lista_clienti.add(aux_client);
			id=id+1;
		}
	}
	
	public double genereazaUnNumarRandom(int numar) { 
		double rezultat=0;
		Random aleator=new Random();
		rezultat=aleator.nextInt(numar)+random_aux;
		return rezultat;
	}

	public int getTimp_magazin() {
		return timp_magazin;
	}

	public void setTimp_magazin(int timp_magazin) {
		this.timp_magazin = timp_magazin;
	}

	public int getTimp_coada() {
		return timp_coada;
	}

	public void setTimp_coada(int timp_coada) {
		this.timp_coada = timp_coada;
	}

	public int getNumarClienti() {
		return numarClienti;
	}

	public void setNumarClienti(int numarClienti) {
		this.numarClienti = numarClienti;
	}

	public BlockingQueue<Client> getLista_clienti() {
		return lista_clienti;
	}

	public void setLista_clienti(BlockingQueue<Client> lista_clienti) {
		this.lista_clienti = lista_clienti;
	}

	public int getRandom_aux() {
		return random_aux;
	}


	

}
