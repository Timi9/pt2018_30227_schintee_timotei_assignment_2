package graphicUserInterface;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.Timer;

import casa_magazin.CasaMagazin;
import client.Client;
import client.PopulareDeClienti;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class InterfataGrafica {

	private JFrame frame;
	public JTextField textField_timpMagazin;
	public JTextField textField_timpCoada;
	private JTextPane textPane = new JTextPane();
	ReentrantLock lacat_pentru_sincronizare = new ReentrantLock();
	CasaMagazin casa_de_marcat, casa_de_marcat2;
	JComboBox<Integer> comboBox;
	public PopulareDeClienti populare;
	BlockingQueue<Client> clients;
	public int TOTAL_CASA2;
	public int TOTAL_CASA1;
	
	public JTextPane getTextPane() {
		return textPane;
	}

	public void setTextPane(JTextPane textPane) {
		this.textPane = textPane;
	}

	Color c=new Color(34, 23,10, 40);
	Color c1=new Color(14, 53,50, 30);
	
	/*public void introducere_valori_lista() {
		this.populare=new PopulareDeClienti(5,5,5);
	}*/
	public JLabel lblNewLabel_casa1 = new JLabel("");
	public JLabel lblNewLabel_casa2 = new JLabel("");
	
	public InterfataGrafica() {
	    initialize();
	
	
		frame.setVisible(true);
		
	}

	public void initialize() {
									      
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.info);
		frame.setBackground(Color.PINK);
		frame.setBounds(100, 100, 738, 434);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		textPane.setBackground(new Color(0, 255, 255));
		textPane.setBounds(298, 11, 362, 358);
		frame.getContentPane().add(textPane);
		
		textField_timpMagazin = new JTextField();
		textField_timpMagazin.setBounds(125, 98, 86, 20);
		frame.getContentPane().add(textField_timpMagazin);
		textField_timpMagazin.setColumns(10);
		textField_timpCoada = new JTextField();
		textField_timpCoada.setBounds(125, 143, 86, 20);
		frame.getContentPane().add(textField_timpCoada);
		textField_timpCoada.setColumns(10);
		
		JRadioButton rdbtnRunapp = new JRadioButton("Simulare");
		
		
		rdbtnRunapp.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		rdbtnRunapp.setBackground(new Color(199, 21, 133));
		rdbtnRunapp.setBounds(151, 298, 107, 23);
		frame.getContentPane().add(rdbtnRunapp);
		
		JLabel lblIntroducet = new JLabel("Introduceti parametrii simularii");
		lblIntroducet.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblIntroducet.setForeground(new Color(0, 0, 0));
		lblIntroducet.setBounds(22, 28, 236, 34);
		frame.getContentPane().add(lblIntroducet);
		
		JLabel lblTimpmagazin = new JLabel("Timp Magazin");
		lblTimpmagazin.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblTimpmagazin.setBounds(10, 101, 105, 17);
		frame.getContentPane().add(lblTimpmagazin);
		
		JLabel lblTimpCoada = new JLabel("Timp coada");
		lblTimpCoada.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 13));
		lblTimpCoada.setBounds(10, 146, 105, 17);
		frame.getContentPane().add(lblTimpCoada);
		
		comboBox = new JComboBox<Integer>();
		comboBox.setModel(new DefaultComboBoxModel<Integer>(new Integer[] {6,7,8,10,12,13,15}));
		comboBox.setBounds(123, 221, 135, 22);
		/*comboBox.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				if(comboBox.getSelectedItem().toString().equals("MultyWithScalar")){
					txtrrPolynom.setVisible(true);	
					txtpnPolynom.setText("Scalar");
					polynom2.setVisible(false);
				}
			
			}
		});*/
		frame.getContentPane().add(comboBox);
		
		
		lblNewLabel_casa1.setBounds(69, 345, 142, 14);
		frame.getContentPane().add(lblNewLabel_casa1);
	
		lblNewLabel_casa2.setBounds(69, 369, 142, 14);
		frame.getContentPane().add(lblNewLabel_casa2);
		
		JLabel lblNrClienti = new JLabel("Nr Clienti");
		lblNrClienti.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblNrClienti.setBounds(10, 224, 80, 14);
		frame.getContentPane().add(lblNrClienti);
		
		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				if(rdbtnRunapp.isEnabled()==true) {
					System.err.println("INCEPE SIMULAREA!");
				clients = new LinkedBlockingQueue<Client>();

				populare=new PopulareDeClienti(Integer.parseInt(comboBox.getSelectedItem().toString()),Integer.parseInt(textField_timpMagazin.getText()),Integer.parseInt(textField_timpCoada.getText()));
				clients=populare.getLista_clienti();
					
				//initializam threadurile
				casa_de_marcat=new CasaMagazin(1,lacat_pentru_sincronizare,clients);
				casa_de_marcat2=new CasaMagazin(2,lacat_pentru_sincronizare,clients);
				
				int delay = 65; 
						// pentru afisare in GUI pe textul ala
			      ActionListener taskPerformer = new ActionListener() {
			          public void actionPerformed(ActionEvent evt) {	        
			        	  textPane.setText(casa_de_marcat.mesaj_afisare_consola);   
			        	  textPane.setText(casa_de_marcat2.mesaj_afisare_consola);   
			          }
			      };
			     new Timer(delay, taskPerformer).start();
			     
			     
				casa_de_marcat.start();  // pornesti executie primul thread
				casa_de_marcat2.start(); //pornesti executie al doilea thread
				}
				TOTAL_CASA1=casa_de_marcat.getTotal_clienti_veniti_la_casa();
				TOTAL_CASA2=casa_de_marcat2.getTotal_clienti_veniti_la_casa();
				
				System.out.println(casa_de_marcat.getTotal_clienti_veniti_la_casa());
				System.out.println(casa_de_marcat2.getTotal_clienti_veniti_la_casa());
	
			}
		});
		
		btnStart.setBackground(new Color(255, 0, 0));
		btnStart.setForeground(new Color(0, 0, 0));
		btnStart.setFont(new Font("Yu Gothic UI", Font.BOLD | Font.ITALIC, 11));
		btnStart.setBounds(22, 300, 89, 23);
		frame.getContentPane().add(btnStart);
		
		JLabel lblCasa = new JLabel("Casa1");
		lblCasa.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblCasa.setBackground(Color.YELLOW);
		lblCasa.setBounds(22, 344, 46, 17);
		frame.getContentPane().add(lblCasa);
		
		JLabel lblCasa_1 = new JLabel("Casa2");
		lblCasa_1.setFont(new Font("Times New Roman", Font.BOLD, 12));
		lblCasa_1.setBounds(22, 369, 46, 14);
		frame.getContentPane().add(lblCasa_1);
		
		
	}
}
