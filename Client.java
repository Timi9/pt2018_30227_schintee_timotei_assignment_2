package client;

public class Client {
	
	private double TimpInMagazin;
	private double TimpLaCoada;
	private int id;
	
	
	public Client( int id,double maxTimpInMagazin, double maxTimpLaCoada) {
		super();
		this.TimpInMagazin = maxTimpInMagazin;
		this.TimpLaCoada = maxTimpLaCoada;
		this.id = id;
	}
	public Client() {
		
	}
	
	/**
	 * @return the maxTimpInMagazin
	 */
	public double getMaxTimpInMagazin() {
		return TimpInMagazin;
	}
	/**
	 * @param maxTimpInMagazin the maxTimpInMagazin to set
	 */
	public void setMaxTimpInMagazin(double maxTimpInMagazin) {
		this.TimpInMagazin = maxTimpInMagazin;
	}
	/**
	 * @return the maxTimpLaCoada
	 */
	public double getMaxTimpLaCoada() {
		return TimpLaCoada;
	}
	/**
	 * @param maxTimpLaCoada the maxTimpLaCoada to set
	 */
	public void setMaxTimpLaCoada(double maxTimpLaCoada) {
		this.TimpLaCoada = maxTimpLaCoada;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public String toString() {
		return "Client:"+this.id;
	}


}
